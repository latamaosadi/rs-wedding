<div class="navbar-container w-full bg-blue-lightest z-20">
  <ul class="list-reset h-16 md:h-24 flex justify-center items-center">
    <li class="mr-2 md:mr-4">
      <?= view('navbar.button', [
        'title' => trans('label.invitation'),
        'anchor' => '#invitation',
        // 'icon' => view('navbar.icon.invitation'),
        'icon' => '',
      ]); ?>
    </li>
    <li class="mr-2 md:mr-4">
      <?= view('navbar.button', [
        'title' => trans('label.our_story'),
        'anchor' => '#story',
        // 'icon' => view('navbar.icon.story'),
        'icon' => '',
      ]); ?>
    </li>
    <li class="mr-2 md:mr-4">
      <?= view('navbar.button', [
        'title' => trans('label.rsvp'),
        'anchor' => '#rsvp',
        // 'icon' => view('navbar.icon.story'),
        'icon' => '',
      ]); ?>
    </li>
    <li class="mr-2 md:mr-4">
      <?= view('navbar.button', [
        'title' => trans('label.location'),
        'anchor' => '#location',
        // 'icon' => view('navbar.icon.location'),
        'icon' => '',
      ]); ?>
    </li>
  </ul>
</div>