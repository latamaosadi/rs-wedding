<div class="py-2 px-4">
  <h3 class="mb-4 text-center">Ceremony</h3>
  <a href="https://www.google.com/maps/dir/?api=1&destination=<?= config('config.ceremony.location.lat'); ?>,<?= config('config.ceremony.location.lng'); ?>" target="_blank" class="bg-blue hover:bg-blue-light text-white py-2 px-4 border-b-4 border-blue-dark hover:border-blue rounded inline-block">Start Navigation <i class="fas fa-directions text-lg align-middle"></i></div>
</div>