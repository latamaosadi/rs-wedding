<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Robert & Steffi's Wedding</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[]; w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'}); var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:''; j.async=true; j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl; f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-N9ZJS2L');</script>
  <!-- End Google Tag Manager -->

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

  <link href="https://fonts.googleapis.com/css?family=Indie+Flower|Merriweather:400,900" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/preflight.min.css" rel="stylesheet">
  <link href="https://api.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.css" rel="stylesheet" />
  <link rel="stylesheet" href="/css/app.css">

  <link href="https://cdn.jsdelivr.net/npm/tailwindcss/dist/utilities.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body class="bg-blue-lightest text-blue-darkest">

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N9ZJS2L" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

  <div class="container">

    <!-- Heading -->
    <?= view('heading'); ?>

    <!-- Navbar -->
    <?= view('navbar'); ?>

    <!-- Banner -->
    <?= view('banner'); ?>

    <!-- Body -->
    <div class="w-full body-container bg-white relative">
      <div class="bg-container z-0"></div>
      <div class="bg-white mx-auto max-w-xl shadow-lg z-10 relative">
        <!-- Invitation -->
        <?= view('section.invitation'); ?>

        <!-- Biography -->
        <?= view('section.story'); ?>

        <!-- RSVP -->
        <?= view('section.rsvp'); ?>

        <!-- Map -->
        <?= view('section.map'); ?>
      </div>
    </div>

    <div class="fixed mb-6 mr-6 nav-top pin-b pin-r hidden z-30">
      <a href="#top" class="bg-white flex h-10 items-center justify-center rounded-full shadow-lg text-4xl w-10"><i class="fas fa-angle-up"></i></a>
    </div>

  </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://api.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js"></script>
  <script src="/vendor/scrollify/jquery.scrollify.js"></script>
  <script src="/vendor/parallaxjs/parallax.min.js"></script>
  <script type="text/javascript">
    $(function () {
      var navbarEl = $('.navbar-container');
      var navbarPosition = navbarEl.position().top;
      var navbarHeight = navbarEl.outerHeight();

      function compareScrollFn (element) {
        if ($(element).scrollTop() > navbarPosition) {
          navbarEl.addClass('fixed pin-t shadow-lg');
          $('.nav-top').removeClass('hidden');
          $('.heading').css({
            'marginBottom': navbarHeight
          });
          return;
        }

        navbarEl.removeClass('fixed pin-t shadow-lg');
        $('.nav-top').addClass('hidden');

        $('.heading').css({
          'marginBottom': 0
        });
      }

      compareScrollFn(document);

      $(document).on('scroll', function (event) {
        var _self = this;

        compareScrollFn(_self);

      });

      $.scrollify({
        sectionName: 'section-name',
        setHeights: false,
        updateHash: false,
        scrollbars: true,
        section: '.section-container',
        standardScrollElements: '#location',
        before: function (index, sections) {
          var activeSection = sections[index];
          var id = $(activeSection).attr('id');
          $('.navbar-container .navbar-item').removeClass('active');
          if (! id) {
            return;
          }
          $('.navbar-container .navbar-item[href="#' + id + '"]').addClass('active');
        },
      });

      function gotoSectionFn (el) {
        $.scrollify.move($(el).attr('href'));
      }

      $('.navbar-container').on('click', '.navbar-item', function (event) {
        event.preventDefault();
        var _self = this;
        gotoSectionFn(_self);
      });

      $('.nav-top').on('click', 'a', function (event) {
        event.preventDefault();
        var _self = this;
        gotoSectionFn(_self);
      });

    });
  </script>
  <script>
    mapboxgl.accessToken = 'pk.eyJ1IjoibGF0YW1hb3NhZGkiLCJhIjoiY2pnN2IzbXJ2M3c1cDJ3cGNpamQwZHRydyJ9.zNH7_dUIB83NBdATeX2_oA';
    var receptionLocation = {'lat': <?= config('config.reception.location.lat'); ?>, 'lng': <?= config('config.reception.location.lng'); ?>};
    var ceremonyLocation = {'lat': <?= config('config.ceremony.location.lat'); ?>, 'lng': <?= config('config.ceremony.location.lng'); ?>};
    var popupConfig = {
      anchor: 'bottom',   // To show popup on top
      offset: { 'bottom': [0, -10] },  // To prevent popup from over shadowing the marker.
      closeOnClick: false,   // To prevent close on mapClick.
      closeButton: false,
      popupOpen: true,
    };

    var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/latamaosadi/cjra10xl01v6v2smsqc9k4i06',
      center: receptionLocation,
      scrollZoom: false,
      zoom: 16,
    });

    map.addControl(new mapboxgl.NavigationControl({
      'showCompass': false,
    }));

    map.fitBounds([receptionLocation, ceremonyLocation]);
    setTimeout(function () {
      map.setZoom(15);
    }, 300);

    var popupReception = new mapboxgl.Popup(popupConfig)
      .setHTML('<?= trim(preg_replace('~[\r\n]+~', '', view('popup.reception'))); ?>');
    var popupCeremony = new mapboxgl.Popup(popupConfig)
      .setHTML('<?= trim(preg_replace('~[\r\n]+~', '', view('popup.ceremony'))); ?>');

    var el = document.createElement('div');
    el.className = 'marker reception border-4 border-blue';

    var receptionMarker = new mapboxgl.Marker(el, {offset: [0, 0]})
      .setLngLat(receptionLocation)
      .setPopup(popupReception)
      .addTo(map);

    el = document.createElement('div');
    el.className = 'marker ceremony border-4 border-blue';

    var ceremonyMarker = new mapboxgl.Marker(el, {offset: [0, 0]})
      .setLngLat(ceremonyLocation)
      .setPopup(popupCeremony)
      .addTo(map);
  </script>
</body>
</html>