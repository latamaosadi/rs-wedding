<a class="inline-block text-black py-2 px-2 md:px-4 navbar-item text-xs md:text-lg font-light relative" href="<?= $anchor; ?>">
  <?php if (isset($icon) && ! empty($icon)): ?>
  <span class="mr-2"><?= $icon; ?></span>
  <?php endif; ?>
  <span class="uppercase"><?= $title; ?></span>
</a>