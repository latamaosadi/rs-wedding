<div id="top" class="w-full bg-white text-center py-16 px-6 md:px-16 section-container heading" data-section-name="top">
  <h1 class="mb-6 font-light cursive md:text-5xl">-Robert <span class="text-blue-dark font-black">&</span> Steffi-</h1>
  <h3 class="cursive">April 6, 2019 &bull; Sanur, Bali</h3>
</div>