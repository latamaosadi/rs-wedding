<div id="invitation" class="bg-white section-container pt-16 md:pt-24 std-scroll" data-section-name="invitation">
  <div class="py-4 px-10">
    <h1 class="text-black text-right font-extrabold"><?= trans('label.invitation'); ?></h1>

    <div class="flex flex-col">
      <div class="flex-1 text-center text-xl px-0 py-4 md:p-6">
        <div class="shadow-md rounded-lg overflow-hidden">
          <div class="flex flex-col md:flex-row items-center">
            <div class="w-full md:w-3/5">
              <div class="fit-container wide">
                <img src="/images/ceremony-cover.jpg" class="cover" alt="">
              </div>
            </div>
            <div class="p-4 flex-1">
              <h3 class="font-light text-4xl">The Ceremony</h3>
              <p class="mt-4">Saturday, 6 April 2019</p>
              <p class="mt-4">16:00 - 17:00</p>
              <p class="mt-4 text-sm">Jl. Sudamala No.20, Sanur Kauh, Denpasar Sel., Kota Denpasar</p>
            </div>
          </div>
        </div>
      </div>
      <div class="flex-1 text-center text-xl px-0 py-4 md:p-6">
        <div class="shadow-md rounded-lg overflow-hidden">
          <div class="flex flex-col-reverse md:flex-row items-center">
            <div class="p-4 flex-1">
              <h3 class="font-light text-4xl">The Reception</h3>
              <p class="mt-4">Saturday, 6 April 2019</p>
              <p class="mt-4">19:00 - 21:00</p>
              <p class="mt-4 text-sm">Jl. Sudamala No.20, Sanur Kauh, Denpasar Sel., Kota Denpasar</p>
            </div>
            <div class="w-full md:w-3/5">
              <div class="fit-container wide">
                <img src="/images/reception-cover.jpg" class="cover" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>