<div id="story" class="min-h-screen bg-indigo-darkest section-container pt-16 md:pt-24" data-section-name="story">
  <div class="py-4 px-10">
    <h1 class="text-white text-left font-extrabold"><?= trans('label.our_story'); ?></h1>
  </div>
</div>