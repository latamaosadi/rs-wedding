<div id="location" class="h-screen bg-purple-darker section-container pt-16 md:pt-24 std-scroll" data-section-name="location">
  <div id="map" class="w-full h-full"></div>
</div>