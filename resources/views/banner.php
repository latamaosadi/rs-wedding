<div class="sm:flex sm:items-center px-6 py-4 text-white section-container banner h-screen relative" data-parallax="scroll" data-image-src="/images/header-2.jpg">
  <div class="absolute pin-b pin-l pin-r h-16 bg-white"></div>
</div>