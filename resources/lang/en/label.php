<?php

return [
  'top' => 'Back to Top',
  'invitation' => 'Invitation',
  'our_story' => 'Our Story',
  'rsvp' => 'RSVP',
  'location' => 'Location',
];