<?php

return [
  'top' => 'Ke Atas',
  'invitation' => 'Undangan',
  'our_story' => 'Cerita Kami',
  'rsvp' => 'RSVP',
  'location' => 'Lokasi',
];