<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function home(Request $request)
    {
        $locale = $request->input('lang', 'en');
        app('translator')->setLocale($locale);
        return view('home');
    }
}