<?php

return [
  'reception' => [
    'name' => 'Sudamala Suites & Villa',
    'phone' => '(0361) 288555',
    'address' => 'Jl. Sudamala No.20, Sanur Kauh, Denpasar Sel., Kota Denpasar, Bali 80227, Indonesia',
    'location' => [
      'lat' => '-8.7077495',
      'lng' => '115.256035',
    ],
  ],
  'ceremony' => [
    'name' => 'Sudamala Suites & Villa',
    'location' => [
      'lat' => '-8.711248',
      'lng' => '115.255642',
    ],
  ],
];
